package ua.kiev.prog;

import java.util.LinkedList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MessageList { //класс сделан в виде Синглтона =
	// = класс кот. может иметь только 1объект
	private static final MessageList msgList = new MessageList();

    private final Gson gson;

	//все сообщения складируются в LinkedList<>()
	//у каждого сообщ.есть свой номер, кот.=индексу сообщ.в листе
	private final List<Message> list = new LinkedList<>();
	
	public static MessageList getInstance() {
		return msgList;
	}
  
  	private MessageList() {
		gson = new GsonBuilder().create();
	}
	
	public synchronized void add(Message m) {
		list.add(m);
	}
	
	public synchronized String toJSON(int n) {
		if (n >= list.size()) return null;
		return gson.toJson(new JsonMessages(list, n));
	}
}
