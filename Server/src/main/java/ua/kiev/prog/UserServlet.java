package ua.kiev.prog;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class UserServlet extends HttpServlet {
    private UserList userList = UserList.getInstance();
    private List <User> userl = userList.getListuser();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        byte[] buf = requestBodyToArray(req);
        String bufStr = new String(buf, StandardCharsets.UTF_8);

        User user = User.fromJSON(bufStr);
        if (user != null) {
            for (int i = 0; i < userl.size(); i++) {
                if (userl.get(i).getLogin().equals(user.getLogin()) && userl.get(i).getPassword().equals(user.getPassword())) {
                    resp.setStatus(HttpServletResponse.SC_CREATED);
                    return;
                } else if (userl.get(i).getLogin().equals(user.getLogin()) && (!userl.get(i).getPassword().equals(user.getPassword()))) {
                    resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                    return;
                }
            }
            userList.add(user);
        }else
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    private byte[] requestBodyToArray(HttpServletRequest req) throws IOException {
        InputStream is = req.getInputStream();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[10240];
        int rb;

        do {
            rb = is.read(buf);
            if (rb > 0)
                bos.write(buf, 0, rb);
        } while (rb != -1);

        return bos.toByteArray();
    }
}
