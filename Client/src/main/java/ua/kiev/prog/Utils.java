package ua.kiev.prog;

import java.io.IOException;
import java.util.Scanner;

public class Utils {
    private static final String URL = "http://127.0.0.1";
    private static final int PORT = 8080;

    public static String getURL() {
        return URL + ":" + PORT;
    }

    public static User logIn() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your login:");
        String login = sc.nextLine();
        System.out.println("Enter your password:");
        String password = sc.nextLine();

        User user = new User (login, password);
        int num = user.userIs();
        if (num != 201) {
            System.out.println("Welcome!");
        } else {
            System.out.println("Input correct login and/or password, please!");
        }
        return user;
    }

    private static void msg (User user) {
        Scanner scanner = new Scanner(System.in);

        Thread th = new Thread(new GetThread());
        th.setDaemon(true);
        th.start();


        System.out.println("Enter your message: ");
        while (true) {
            String text = scanner.nextLine();
            if (text.isEmpty()) break;

            Message m = new Message (user.getLogin(), text);
            int res = 0;
            try {
                res = m.send(Utils.getURL() + "/add");
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (res != 200) { // 200 OK
                System.out.println("HTTP error occured: " + res);
                return;
            }
        }
    }

    private static void privateMsg (User user) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter your name for private: ");
            String to = scanner.nextLine();
            Thread th = new Thread(new GetThrdPrvtMsgs());
            th.setDaemon(true);
            th.start();

            System.out.println("Enter your message: ");
            while (true) {
                String text = scanner.nextLine();
                if (text.isEmpty()) break;

                PrivateMsgs m = new PrivateMsgs(user.getLogin(), to, text);
                int res = m.send(Utils.getURL() + "/add");

                if (res != 200) {
                    System.out.println("HTTP error: " + res);
                    return;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void chat(User user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input 1: General chat");
        System.out.println("Input 2: Private chat");
        int ch = scanner.nextInt();
        switch (ch) {
            case 1 -> msg(user);
            case 2 -> privateMsg(user);
            default -> {
            }
        }
    }
}
